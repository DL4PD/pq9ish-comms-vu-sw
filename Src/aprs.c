/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2018 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pq9ish.h>
#include "aprs.h"
#include "stm32l4xx_hal.h"
#include <string.h>
#include <stdio.h>

extern RTC_HandleTypeDef hrtc;

static uint8_t __aprs_report[256];

int
aprs_init(aprs_conf_t *conf,
          const uint8_t *dest_addr,
          uint8_t dest_ssid,
          const uint8_t *src_addr,
          uint8_t src_ssid,
          aprs_type_t type,
          const char *lat,
          const char *lon)
{
  int ret;

  if(!conf || !lon || !lat) {
    return -PQ9ISH_INVALID_PARAM;
  }

  /* Sanity checks for lon/lat coordinates */
  if(strnlen(lon, APRS_LON_STR_LEN) != APRS_LON_STR_LEN
      || strnlen(lat, APRS_LAT_STR_LEN) != APRS_LAT_STR_LEN) {
    return -PQ9ISH_INVALID_PARAM;
  }

  memcpy(conf->lon, lon, APRS_LON_STR_LEN);
  memcpy(conf->lat, lat, APRS_LAT_STR_LEN);

  ret = ax25_init(&conf->hax25, dest_addr, dest_ssid, src_addr, src_ssid,
                  AX25_PREAMBLE_LEN, AX25_POSTAMBLE_LEN,
                  AX25_UI_FRAME, 0x3, 0xF0);
  if(ret) {
    return ret;
  }

  return PQ9ISH_SUCCESS;
}


/**
 * Get an APRS compatible (DHM) weather timestamp in UTC
 * @param out the string to hold the timestamp. The string should be
 * at least 8 bytes according to the APRS specification (7 digits + null termination).
 * The resulting string is a valid null terminated string.
 * @return
 */
static int
aprs_get_timestamp(char *out)
{
  int ret;
  RTC_DateTypeDef date;
  RTC_TimeTypeDef time;
  if(!out) {
    return -PQ9ISH_INVALID_PARAM;
  }
  ret = HAL_RTC_GetTime(&hrtc, &time, RTC_FORMAT_BIN);
  if(ret) {
    return ret;
  }
  ret = HAL_RTC_GetDate(&hrtc, &date, RTC_FORMAT_BIN);
  if(ret) {
    return ret;
  }
  sprintf(out, "%02u", date.Date);
  sprintf(out + 2, "%02u", time.Hours);
  sprintf(out + 4, "%02u", time.Minutes);
  out[6] = 'z';
  out[7] = 0;
  return PQ9ISH_SUCCESS;
}


int
aprs_tx_packet(aprs_conf_t *conf, ax5043_conf_t *hax5043,
                       const aprs_data_t *ad)
{
  int ret;
  uint32_t report_len = 0;
//  char timestamp[8];

/*  ret = aprs_get_timestamp(timestamp);
  if(ret) {
    return ret;
  }*/

  ret = sprintf ((char *) __aprs_report + report_len,
                 "T#%03d,%03d,%03d,%03d,%03d,%03d,%03d", ad->sequence, ad->aval1,
                 ad->aval2, ad->aval3, ad->aval4, ad->aval5, ad->dval);
  if(ret < 1) {
    return -PQ9ISH_INVALID_PARAM;
  }
  report_len += (uint32_t)ret;


  return ax25_tx_frame(&conf->hax25, hax5043, __aprs_report, report_len);
}

