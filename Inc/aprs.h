/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2018 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef APRS_H_
#define APRS_H_

#include "ax25.h"
#include "ax5043.h"

#define APRS_LON_STR_LEN                (9)
#define APRS_LAT_STR_LEN                (8)

typedef enum
{
  APRS_WEATHER = 0
} aprs_type_t;

typedef struct
{
  aprs_type_t   type;
  ax25_conf_t   hax25;
  char          lon[APRS_LON_STR_LEN];
  char          lat[APRS_LAT_STR_LEN];
} aprs_conf_t;

typedef struct
{
  uint16_t      sequence;
  uint8_t       aval1;
  uint8_t       aval2;
  uint8_t       aval3;
  uint8_t       aval4;
  uint8_t       aval5;
  uint8_t       aval6;
  uint8_t       aval7;
  uint8_t       aval8;
  uint8_t       dval;
} aprs_data_t;

int
aprs_init(aprs_conf_t *conf,
          const uint8_t *dest_addr,
          uint8_t dest_ssid,
          const uint8_t *src_addr,
          uint8_t src_ssid,
          aprs_type_t type,
          const char *lat,
          const char *lon);

int
aprs_tx_packet(aprs_conf_t *conf, ax5043_conf_t *hax5043,
                       const aprs_data_t *w);

#endif /* APRS_H_ */
